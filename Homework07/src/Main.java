import VM.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        VM vm = new VM();
        int money = 0;
        while (true) {
            vm.table1();
            int choice1 = s.nextInt();
            switch (choice1) {
                case 1:
                    vm.table2();
                    break;
                case 2:
                    System.out.println("\nEnter the money");
                    money = s.nextInt();
                    vm.enteringMoney(money);
                    break;
                case 3:
                    System.out.println("\nChoose a drink");
                    int choice2 = s.nextInt();
                    switch (choice2) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                            vm.deliveryOfGoods(choice2, money);
                            money = 0;
                            break;
                        default:
                            System.out.println("Incorrect numder. Get your money.");
                            money = 0;
                    }
                    break;
                default:
                    System.out.println("Incorrect numder.");
            }
        }

    }
}
