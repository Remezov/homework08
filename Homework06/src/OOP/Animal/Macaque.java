package OOP.Animal;

public class Macaque extends Monkey {
    public Macaque(String name, String action) {
        super(name, action);
    }

    @Override
    public String makeSound()
    {
        return "I " +this.action+ "!";
    }
}
