package OOP.Animal;

public abstract class Monkey extends Animal {
    public String action;

    public Monkey(String name, String action) {
        super(name);
        this.action = action;
    }

    public String call()
    {
        return this.name+" said: "+this.makeSound()+"\n";
    }
}
